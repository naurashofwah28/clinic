<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="{{ asset('css/welcome.css') }}" rel="stylesheet" type="text/css">
    
</head>
<div class="medclinic">myClinic</div>
<div class="home">
    <a href='/' class="button" style="color: black">Home</a>
</div>
<body class="antialiased">
    <div class="db">
        @if (Route::has('login'))
            <div class="flex justify-end"> <!-- Menambahkan class "flex justify-end" -->
                @auth
                    <a href="{{ url('/dashboard') }}" class="dashboard">Dashboard</a>
                @else
                    <div class="home1">
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="button signup-button" style="color: #0085be;">Sign Up</a>
                        @endif
    
                        <a href="{{ route('login') }}" class="button login-button">Log In</a> <!-- Menukar posisi Log in dengan Sign Up -->
                        <a href="{{ route('register') }}" class="button register2" style="color: rgb(255, 255, 255);">REGISTER NOW</a>
                    </div>
                @endauth
            </div>
        @endif
    </div>

    <div class="content">

        <div class="welcome">Welcome To</div>
        <div class="meet">
            <p>The Best Medical Clinic</p>
        </div>
        <div class="silahkan">
            <p class="first-line">Providing Quality <span class="blue-text">Healthcare</span> for a</p>
            <p class="second-line">
                <span class="blue-text">Brighter</span> and <span class="blue-text">Healthy</span> Future
            </p>
        </div>
        </div>
        <div class="image">
            <img src="img/imagewelcome.png" alt="image" width="650px" height="650px">
        </div>
        <div class="image2">
            <img src="img/image-14.png" alt="image" width="112" height="189">
        </div>
    

</body>
</html>
