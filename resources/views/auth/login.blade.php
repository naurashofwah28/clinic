<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="medclinic">myClinic</div>
    <div class="image3">
        <img src="img/er.png" alt="image3">
    </div>
    <h1>Masuk</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <table>
              {{-- <tr>
                <td>
                    <label for="email">Email</label>
                </td> --}}
                {{-- <td> --}}
                    <input type="email" name="email" id="email" style="width: 100%;" placeholder="Masukkan Email" class="input">
                {{-- </td>
            </tr> --}}
            {{-- <tr>
                <td>
                    <label for="password">Password</label>
                </td>
                <td> --}}
                    <input type="password" name="password" id="password" style="width: 100%;" placeholder="Masukkan Password" class="input">
                {{-- </td>
            </tr>
            <tr> --}}
                <td colspan="2">
                    <button class="button register-button">Login</button>
                </td>
            </tr>
            @if ($errors->any())
            <tr>
                <td colspan="2" align="center">
                    <div class="error-message">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span><br>
                        @endforeach
                    </div>
                </td>
            </tr>
            @endif
            <tr>
                <td colspan="2" align="center">
                    <a href="/register">Klik Jika Belum Mempunyai Akun</a>
                </td>
            </tr>
        </table>
    </form>

    <div class="home">
        <a href='/' class="button" style="color: black">Home</a>
    </div>
    <div class="home1">
        <a href="register" class="button signup-button" style="color: #0085be;">Sign Up</a>
        <a href="login" class="button login-button">Log In</a>
    </div>

    <div class="image">
        <img src="img/imageregis.png" alt="image">
    </div>
    <div class="image2">
        <img src="img/image-14.png" alt="image" width="100" height="189">
    </div>
</body>
</html>
