<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = 'antrian';
    protected $primaryKey = 'noAntrian';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'noAntrian',
        'nama',
        'idUser',
        'kategori',
        'poli',
        'noHP',
        'status'
    ];
}
